---
title: "3670.Lab06_N"
type: build
url: /builds/3670
build_tag: "6.0.3670.0.Lab06_N.020819-1749"
build_arch: [ "x86" ]
build_m: 2
---

This build is not much newer than 3663 and still not much of the UI had been changed at this time. One of the screenshots shows the all new device manager that was integrated in explorer.

### Gallery

{{< gallery "desktop" "winver" "explorer" "explorer-classic" "overview" "my-hardware" "msn" >}}