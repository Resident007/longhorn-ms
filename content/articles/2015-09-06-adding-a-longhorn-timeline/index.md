---
title: Timeline
author: Melcher
type: post
date: 2015-09-06T16:11:10+00:00
url: /adding-a-longhorn-timeline
categories:
  - Site news

---
The brand new [timeline page](/timeline) has been published! The timeline is the place where all important Longhorn events will be bookmarked. Though the timeline remains unfinished, additional events will be added over time. If you have something to add be sure to contact us. Have a look and let us now what you think about this new format in the comments.