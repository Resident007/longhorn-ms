---
title: New look
author: Melcher
type: post
date: 2015-06-21T13:19:23+00:00
url: /new-look
categories:
  - Site news
format: aside

---
Longhorn.ms is getting a new look! We are working on a more organised style for the future. While the site under construction it is possible that some pages cannot be reached.