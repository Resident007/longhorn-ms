---
title: "Experience Longhorn"
---

# "Long what?"

Longhorn is the codename for the operating system that was to be Windows XP's successor. At first, Longhorn was planned to be a minor release, with only a small amount of new features. Soon after the project gained momentum it was decided to make Longhorn a major release.

The project, however, failed completely and development had to reset. More than three years late Microsoft released Windows Code Name 'Longhorn' as Windows Vista.


The goal of Longhorn.ms is to provide all information needed to preserve one of the most extraordinary Microsoft projects, Longhorn.

In the [Builds](/builds/) section you will find BIOS dates, product keys and installation notes to help you install Longhorn today. Moreover, you will also find fixes and interesting facts about each build. In the [Articles](/articles/) section you can find post ranging from elaborate tutorials to technical description of Longhorn features. For the ones less interested in all technical hodgepodge there is the development category completely focused on how Longhorn was being developed at Microsoft.

Longhorn.ms is currently maintained by Thomas Hounsell. You can submit changes via pull requests on [our GitLab repository](https://gitlab.com/hounsell/longhorn-ms).
